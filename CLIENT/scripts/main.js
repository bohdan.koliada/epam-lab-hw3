// reg/login display form part
const containerHeader = document.querySelector('.container-header');
containerHeader.addEventListener('click', (e) => {
  if (e.target.innerHTML === 'Registration') {
    const loginForm = document.querySelector('.login-form');
    const registrationForm = document.querySelector('.registration-form');
    loginForm.classList.add('display-none');
    registrationForm.classList.remove('display-none');
  }
  if (e.target.innerHTML === 'Login') {
    const loginForm = document.querySelector('.login-form');
    const registrationForm = document.querySelector('.registration-form');
    registrationForm.classList.add('display-none');
    loginForm.classList.remove('display-none');
  }
});

// registration part
const buttonRegister = document.querySelector('.button-register');
buttonRegister.addEventListener('click', () => {
  const event = document.querySelector('.select-role');
  const role = event.options[event.selectedIndex].text;
  const email = document.querySelector('.email-registration').value;
  const pass = document.querySelector('.pass-registration').value;

  fetch('http://localhost:8080/api/auth/register', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
    body: JSON.stringify({
      email,
      password: pass,
      role,
    }),
  })
    .then((response) => {
      if (response.ok) {
        response.json().then((data) => {
          document.querySelector('.registration-answer').innerHTML = `${data.message}`;
        });
      } else {
        response.json().then(() => {
          document.querySelector('.registration-answer').innerHTML = 'Registration failed.. Email already exists';
        });
      }
    });
});

// login part + check who is logged in (DRIVER/SHIPPER)
const buttonLogin = document.querySelector('.button-login');
buttonLogin.addEventListener('click', () => {
  const email = document.querySelector('.username-email').value;
  const pass = document.querySelector('.username-pass').value;
  fetch('http://localhost:8080/api/auth/login', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    },
    body: JSON.stringify({
      email,
      password: pass,
    }),
  })
    .then((response) => {
      if (response.ok) {
        response.json().then((token) => {
          localStorage.setItem('token', `${token.jwt_token}`);
          fetch('http://localhost:8080/api/users/me', {
            method: 'GET',
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
              authorization: `Bearer ${localStorage.getItem('token')}`,
            },
          })
            .then((res) => {
              if (res.ok) {
                res.json().then((data) => {
                  if (data.user.role === 'DRIVER') {
                    const url = `http://${window.location.host}/pages/driver-menu.html`;
                    window.location.assign(url);
                  } else {
                    const url = `http://${window.location.host}/pages/shipper-menu.html`;
                    window.location.assign(url);
                  }
                });
              } else {
                res.json().then(() => {
                  console.log('Error');
                });
              }
            });
        });
      } else {
        response.json().then((data) => {
          document.querySelector('.login-answer').innerHTML = `${data.message}`;
        });
      }
    });
});
