const profileInfo = document.querySelector('.profile-info');
const showingTrucks = document.querySelector('.showing-trucks');
const showAddingTruck = document.querySelector('.adding-truck');

// loading user info after login
fetch('http://localhost:8080/api/users/me', {
  method: 'GET',
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
    authorization: `Bearer ${localStorage.getItem('token')}`,
  },
})
  .then((response) => {
    if (response.ok) {
      response.json().then((data) => {
        const driverEmail = document.querySelector('.driver-email');
        driverEmail.innerHTML = `${data.user.email}`;
      });
    } else {
      response.json().then(() => {
        console.log('Error');
      });
    }
  });

// logout part
const buttonLogout = document.querySelector('.button-logout');
buttonLogout.addEventListener('click', () => {
  localStorage.removeItem('token');
  const url = 'http://localhost:5500/index.html';
  window.location.assign(url);
});

// show user profile info
const buttonShowUserInfo = document.querySelector('.button-show-info');
buttonShowUserInfo.addEventListener('click', () => {
  fetch('http://localhost:8080/api/users/me', {
    method: 'GET',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        response.json().then((data) => {
          profileInfo.innerHTML = `
            Email - ${data.user.email}<br>
            Role - ${data.user.role}<br>
            Profile created time - ${data.user.created_date}`;
          profileInfo.classList.remove('display-none');
          showingTrucks.classList.add('display-none');
          showAddingTruck.classList.add('display-none');
        });
      } else {
        response.json().then((err) => {
          console.log(err);
        });
      }
    });
});

// show all users trucks
const buttonShowTrucks = document.querySelector('.button-show-trucks');
buttonShowTrucks.addEventListener('click', () => {
  fetch('http://localhost:8080/api/trucks', {
    method: 'GET',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        response.json().then((data) => {
          if (data.trucks) {
            console.log(data.trucks);
            showingTrucks.innerHTML = '';
            for (let i = 0; i < data.trucks.length; i += 1) {
              const newDiv = document.createElement('div');
              newDiv.innerHTML = `
                      Type - ${data.trucks[i].type}<br>
                      Status - ${data.trucks[i].status}<br>`;
              newDiv.setAttribute('id', `${data.trucks[i]._id}`);
              newDiv.classList.add('truck');
              if (data.trucks[i].assigned_to) {
                newDiv.classList.add('truck-assigned');
              }
              // adding assign button
              const buttonAssignTruck = document.createElement('button');
              buttonAssignTruck.innerHTML = 'Assign';
              buttonAssignTruck.classList.add('button-assign-truck');
              // adding delete button
              const buttonDeleteTruck = document.createElement('button');
              buttonDeleteTruck.innerHTML = 'Delete';
              buttonDeleteTruck.classList.add('button-delete-truck');
              newDiv.appendChild(buttonAssignTruck);
              newDiv.appendChild(buttonDeleteTruck);
              showingTrucks.appendChild(newDiv);
            }
            showingTrucks.classList.remove('display-none');
            profileInfo.classList.add('display-none');
            showAddingTruck.classList.add('display-none');
          } else {
            showingTrucks.innerHTML = '';
            const newDiv = document.createElement('div');
            newDiv.innerHTML = `${data.message}`;
            newDiv.classList.add('truck');
            showingTrucks.appendChild(newDiv);
            showingTrucks.classList.remove('display-none');
            profileInfo.classList.add('display-none');
            showAddingTruck.classList.add('display-none');
          }
        });
      } else {
        response.json().then((err) => {
          console.log(err);
        });
      }
    });
});

// show add new truck menu
const buttonShowAddTruck = document.querySelector('.button-show-add-truck');
buttonShowAddTruck.addEventListener('click', () => {
  showAddingTruck.classList.remove('display-none');
  profileInfo.classList.add('display-none');
  showingTrucks.classList.add('display-none');
});

// adding new truck button
const buttonAddTruck = document.querySelector('.button-add-truck');
buttonAddTruck.addEventListener('click', () => {
  const event = document.querySelector('.select-truck-type');
  const type = event.options[event.selectedIndex].text;
  fetch('http://localhost:8080/api/trucks', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    },
    body: JSON.stringify({
      type,
    }),
  })
    .then((response) => {
      if (response.ok) {
        response.json().then((data) => {
          console.log(data);
        });
      } else {
        response.json().then(() => {
          console.log('Not created');
        });
      }
    });
});

// button for assigning truck to a driver
document.addEventListener('click', (event) => {
  if (event.target && event.target.classList.contains('button-assign-truck')) {
    fetch(`http://localhost:8080/api/trucks/${event.target.parentElement.id}/assign`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (response.ok) {
          response.json().then((data) => {
            console.log(data);
            if (data.oldId) {
              const oldAssignedTruck = document.getElementById(`${data.oldId}`);
              oldAssignedTruck.classList.remove('truck-assigned');
              const newAssignedTruck = document.getElementById(`${event.target.parentElement.id}`);
              newAssignedTruck.classList.add('truck-assigned');
            } else {
              const assignedTruck = document.getElementById(`${event.target.parentElement.id}`);
              assignedTruck.classList.add('truck-assigned');
            }
          });
        } else {
          response.json().then(() => {
            console.log('Not assigned');
          });
        }
      });
  }
});

// button for deleting truck
document.addEventListener('click', (event) => {
  if (event.target && event.target.classList.contains('button-delete-truck')) {
    fetch(`http://localhost:8080/api/trucks/${event.target.parentElement.id}`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (response.ok) {
          const deletedDiv = document.getElementById(`${event.target.parentElement.id}`);
          deletedDiv.parentNode.removeChild(deletedDiv);
          console.log('Truck deleted');
        } else {
          response.json().then((err) => {
            console.log(err);
          });
        }
      });
  }
});
