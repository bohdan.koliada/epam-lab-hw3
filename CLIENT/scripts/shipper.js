fetch('http://localhost:8080/api/users/me', {
  method: 'GET',
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
    authorization: `Bearer ${localStorage.getItem('token')}`,
  },
})
  .then((response) => {
    if (response.ok) {
      response.json().then((data) => {
        document.querySelector('.shipper-email').innerHTML = `${data.user.email}`;
      });
    } else {
      response.json().then(() => {
        console.log('Error');
      });
    }
  });

// logout part
const buttonLogout = document.querySelector('.button-logout');
buttonLogout.addEventListener('click', () => {
  localStorage.removeItem('token');
  const url = 'http://localhost:5500/index.html';
  window.location.assign(url);
});
