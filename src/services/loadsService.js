const { Load } = require('../models/Loads');
const { Truck } = require('../models/Trucks');

// const truckType = [
//   {
//     sprinter: {
//       width: 300,
//       length: 250,
//       height: 170,
//       payload: 1700,
//     },
//   },
//   {
//     smallstraight: {
//       width: 500,
//       length: 250,
//       height: 170,
//       payload: 2500,
//     },
//   },
//   {
//     largestraight: {
//       width: 700,
//       length: 350,
//       height: 200,
//       payload: 4000,
//     },
//   },
// ];
const loadStates = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];

// Add Load for User(abailable only for shipper role)
function addLoad(req, res) {
  const { userId } = req.user;
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  const load = new Load({
    created_by: userId,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });
  load.save()
    .then(() => {
      load.updateOne({
        $push: {
          logs: {
            message: 'Load created successfully',
            time: new Date().toISOString(),
          },
        },
      })
        .then(() => {
          res.status(200).send({ message: 'Load created successfully' });
        });
    });
}

// get the list of loads for authorized user, returns list of completed
// and active loads for Driver and list of all available loads for Shipper
const getLoads = (req, res) => {
  Load.find()
    .then((result) => {
      res.status(200).json({
        loads: result,
      });
    });
};

// get the active load for authorized driver(available only for driver)
const getActiveLoad = (req, res) => {
  Load.find({ assigned_to: req.user.userId })
    .then((result) => {
      res.status(200).json({
        load: result[0],
      });
    });
};

// Iterate to next Load state(available only for driver)
const changeLoadState = (req, res) => {
  Load.find({ assigned_to: req.user.userId })
    .then((result) => {
      if (loadStates.indexOf(result[0].state) !== 3) {
        Load.findByIdAndUpdate(
          result[0]._id,
          {
            $set: { state: loadStates[loadStates.indexOf(result[0].state) + 1] },
            $push: {
              logs: {
                message: `Load state changed to '${loadStates[loadStates.indexOf(result[0].state) + 1]}'`,
                time: new Date().toISOString(),
              },
            },
          },
        )
          .then(() => {
            if (loadStates.indexOf(result[0].state) === 2) {
              Load.findByIdAndUpdate(
                result[0]._id,
                {
                  $set: { status: 'SHIPPED' },
                  $push: {
                    logs: {
                      message: 'Load status changed to \'SHIPPED\'',
                      time: new Date().toISOString(),
                    },
                  },
                },
              )
                .then(() => {
                  Truck.find({ assigned_to: result[0].assigned_to })
                    .then((truck) => {
                      Truck.findByIdAndUpdate(
                        truck[0]._id,
                        { $set: { status: 'IS' } },
                      )
                        .then(() => {
                          res.json({ message: `Load state changed to ${loadStates[loadStates.indexOf(result[0].state) + 1]}` });
                        });
                    });
                });
            } else {
              res.json({ message: `Load state changed to ${loadStates[loadStates.indexOf(result[0].state) + 1]}` });
            }
          });
      } else {
      // Load.findByIdAndUpdate(
      //   result[0]._id,
      //   { $set: { state: loadStates[0]} },
      // )
      // .then(() => {
      //   res.json({ message: `Load state changed to ${loadStates[0]}` });
      // });
        res.json({ message: 'Load already shipped.' });
      }
    });
};

// Get user's Load by id
const getLoadById = (req, res) => {
  Load.findById(req.params.id)
    .then((result) => {
      res.status(200).json({
        load: result,
      });
    });
};

// Update user's load by id (abailable only for shipper role)
const updateLoadById = async (req, res) => {
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  await Load.findByIdAndUpdate(
    req.params.id,
    {
      $set: {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
      },
      $push: {
        logs: {
          message: 'Load details changed succesfully',
          time: new Date().toISOString(),
        },
      },
    },
  );
  res.json({ message: 'Load details changed successfully' });
};

// Delete user's load by id (abailable only for shipper role)
const deleteLoadById = (req, res) => {
  Load.findByIdAndDelete(req.params.id)
    .then(() => {
      res.json({ message: 'Load deleted successfully' });
    });
};

// Post a user's load by id, search for drivers (abailable only for shipper role)
const postLoadById = (req, res) => {
  // changing status to posted
  Load.findByIdAndUpdate(
    req.params.id,
    {
      $set: { status: 'POSTED' },
      $push: {
        logs: {
          message: 'Load status changed to \'POSTED\'',
          time: new Date().toISOString(),
        },
      },
    },
  )
    .then(() => {
    // trying to find truck with assigned driver and with 'IS' status
      Truck.find()
        .then((result) => {
          if (result.length > 0) {
            const availableDrivers = result.filter((elem) => elem.assigned_to && elem.status === 'IS');
            if (availableDrivers.length > 0) {
              // if found setting Truck status to OL, Load status to ASSIGNED
              Truck.findByIdAndUpdate(
                availableDrivers[0]._id,
                { $set: { status: 'OL' } },
              )
                .then(() => {
                  Load.findByIdAndUpdate(
                    req.params.id,
                    {
                      $set: {
                        assigned_to: availableDrivers[0].assigned_to,
                        status: 'ASSIGNED',
                        state: 'En route to Pick Up',
                      },
                      $push: {
                        logs: {
                          message: `Load assigned to driver with id - ${availableDrivers[0].assigned_to}`,
                          time: new Date().toISOString(),
                        },
                      },
                    },
                  )
                    .then(() => {
                      res.status(200).json({
                        message: 'Load posted successfully',
                        driver_found: true,
                      });
                    });
                });
            }
          } else {
            // no truck found, changing Load status to NEW
            Load.findByIdAndUpdate(
              req.params.id,
              {
                $set: { status: 'NEW' },
                $push: {
                  logs: {
                    message: 'Load status changed to \'NEW\'. No driver found',
                    time: new Date().toISOString(),
                  },
                },
              },
            )
              .then(() => {
                res.status(200).json({
                  message: 'Load posted not successfully',
                  driver_found: false,
                });
              });
          }
        });
    });
};

// Get user's Load shipping info by id, returns detailed info about
// shipment for active loads (available only for shipper)
const getLoadShippingInfo = (req, res) => {
  Load.find({ _id: req.params.id })
    .then((resultLoad) => {
      Truck.find({ assigned_to: resultLoad[0].assigned_to })
        .then((resultTruck) => {
          res.json({
            load: resultLoad,
            truck: resultTruck,
          });
        });
    });
};

module.exports = {
  addLoad,
  getLoads,
  getActiveLoad,
  changeLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfo,
};
