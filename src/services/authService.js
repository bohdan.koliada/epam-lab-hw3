const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const { User, userJoiSchema } = require('../models/Users');

// register user func
const registerUser = async (req, res) => {
  const { email, password, role } = req.body;

  try {
    await userJoiSchema.validateAsync({ password });
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });

    return user.save()
      .then(() => res.status(200).send({ message: 'Profile created successfully' }))
      .catch((err) => {
        // next(err);
        res.status(400).json({ message: err.message });
      });
  } catch (err) {
    return res.status(401).json({ message: 'You need to create stronger password' });
  }
};

// login user func
const loginUser = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, role: user.role, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).json({ message: 'Success', jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

// forgot password func
const forgotPassword = async (req, res) => {
  const newPassword = '123456';
  const { email } = req.body;
  const user = await User.find({ email });
  if (user.length > 0) {
    User.findByIdAndUpdate(
      { _id: user[0]._id },
      { $set: { password: await bcrypt.hash(newPassword, 10) } },
    )
      .then(() => {
        const mailTransporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'bohdan.koliada@gmail.com',
            pass: 'pxjevpnjalsqvosk',
          },
        });

        const mailDetails = {
          from: 'bohdan.koliada@gmail.com',
          to: `${email}`,
          subject: 'HW3 Forgot Password',
          text: `Here is your new password - ${newPassword}`,
        };

        mailTransporter.sendMail(mailDetails, (err) => {
          if (err) {
            console.log(err);
          } else {
            res.status(200).json({ message: 'New password sent to your email address' });
          }
        });
      });
  }
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
};
