const { Truck } = require('../models/Trucks');

// add Truck for user
function addTruck(req, res) {
  const { userId } = req.user;
  const { type } = req.body;
  const truck = new Truck({
    created_by: userId,
    type,
  });
  truck.save()
    .then(() => { res.status(200).send({ message: 'Truck created successfully' }); });
}

// get the list of trucks for authorized user(abailable only for driver role)
const getTrucks = (req, res) => {
  Truck.find({ created_by: req.user.userId })
    .then((result) => {
      if (result.length > 0) {
        res.status(200).json({
          trucks: result,
        });
      } else {
        res.status(200).json({ message: 'No trucks added yet' });
      }
    });
};
// Get user's truck by id(abailable only for driver role)
const getTruckById = (req, res) => Truck.findById(req.params.id)
  .then((result) => {
    res.status(200).json({
      truck: result,
    });
  });

// Update user's truck by id (abailable only for driver role)
const updateTruckById = (req, res) => {
  const { type } = req.body;
  return Truck.findByIdAndUpdate(
    { _id: req.params.id, created_by: req.user.userId },
    { $set: { type } },
  )
    .then(() => {
      res.json({ message: 'Truck details changed successfully' });
    });
};

// Delete user's truck by id (abailable only for driver role)
const deleteTruckById = (req, res) => Truck.findByIdAndDelete(req.params.id)
  .then(() => {
    res.json({ message: 'Truck deleted successfully' });
  });

// Assign truck to user by id (abailable only for driver role)
const assignTruckById = (req, res) => {
  Truck.find({ assigned_to: req.user.userId })
    .then((result) => {
      if (result.length > 0) {
        console.log(result);
        Truck.updateOne({ _id: result[0]._id }, { $unset: { assigned_to: '' } })
          .then(() => {
            Truck.findByIdAndUpdate(
              { _id: req.params.id },
              { $set: { assigned_to: req.user.userId } },
            )
              .then(() => {
                res.json({ message: 'New truck assigned successfully', oldId: result[0]._id });
              });
          });
      } else {
        Truck.findByIdAndUpdate(
          { _id: req.params.id },
          { $set: { assigned_to: req.user.userId } },
        )
          .then(() => {
            res.json({ message: 'Truck assigned successfully' });
          });
      }
    });
};

module.exports = {
  addTruck,
  getTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
