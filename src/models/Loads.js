const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  status: {
    type: String,
    default: 'NEW',
    required: true,
  },
  state: {
    type: String,
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: { type: Number },
    length: { type: Number },
    height: { type: Number },
  },
  logs: {
    type: Array,
    required: false,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const Load = mongoose.model('load', loadSchema);

module.exports = {
  Load,
};
