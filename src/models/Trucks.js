const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const Truck = mongoose.model('truck', truckSchema);

module.exports = {
  Truck,
};
