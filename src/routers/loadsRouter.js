const express = require('express');

const router = express.Router();
const {
  addLoad, getLoads, getActiveLoad,
  changeLoadState, getLoadById, updateLoadById,
  deleteLoadById, postLoadById, getLoadShippingInfo,
} = require('../services/loadsService');

const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', authMiddleware, getLoads);

router.post('/', authMiddleware, addLoad);

router.get('/active/', authMiddleware, getActiveLoad);

router.patch('/active/state/', authMiddleware, changeLoadState);

router.get('/:id', authMiddleware, getLoadById);

router.put('/:id', authMiddleware, updateLoadById);

router.delete('/:id', authMiddleware, deleteLoadById);

router.post('/:id/post/', authMiddleware, postLoadById);

router.get('/:id/shipping_info/', authMiddleware, getLoadShippingInfo);

module.exports = {
  loadsRouter: router,
};
