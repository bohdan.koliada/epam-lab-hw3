const express = require('express');

const router = express.Router();
const {
  addTruck, getTrucks, getTruckById, updateTruckById, deleteTruckById, assignTruckById,
} = require('../services/trucksService');

const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, addTruck);

router.get('/', authMiddleware, getTrucks);

router.get('/:id', authMiddleware, getTruckById);

router.put('/:id', authMiddleware, updateTruckById);

router.delete('/:id', authMiddleware, deleteTruckById);

router.post('/:id/assign', authMiddleware, assignTruckById);

module.exports = {
  trucksRouter: router,
};
